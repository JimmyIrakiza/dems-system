package controllers;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import common.JSFMessagers;
import dao.impl.CampImpl;
import dao.impl.LocationImpl;
import domain.Camp;
import domain.Location;

@ManagedBean
@SessionScoped
public class AdminController {
	public Camp camp = new Camp();
	private CampImpl campImpl = new CampImpl();
	private LocationImpl locationImpl = new LocationImpl();
	public List<Location> provinces = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"PROVINCE"},
				"from Location where locationType = 'PROVINCE'");
	public List<Location> districts = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"DISTRICT"},
			"from Location where locationType = 'DISTRICT'");
	public List<Location> sectors = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"SECTOR"},
			"from Location where locationType = 'SECTOR'");
	public List<Location> cells = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"CELL"},
			"from Location where locationType = 'CELL'");
	public String provCode, distrCode, sectCode, cellCode;
	public List<Camp> campsList= campImpl.getListCamps();
	
	
	public String registerCamp() {
		try {
			if(camp.getCampName().isBlank() || camp.getCampName().isEmpty()) {
				JSFMessagers.addErrorMessage("Failed to register, camp name is required ");
				return "/admin/createCamp.xhtml";
			} else if(camp.getCapacity()<15 ) {
				JSFMessagers.addErrorMessage("Failed to register, Camp capacity is below average of 15 families");
				return "/admin/createCamp.xhtml";
			} 
			//Search a location based on value selected in combo box
			Location location = locationImpl.getLocationWithQuery(new String[] {"code"}, new Object[] {cellCode}, "from Location");
			//Set the camp location
			camp.setLocation(location);
			//Camp is saved
			Camp newCamp = campImpl.saveCamps(camp);
			//updating Camp and assign the code
			newCamp.setCampCode("CAMP-"+newCamp.getId());
			campImpl.UpdateCamps(newCamp);
			campsList= campImpl.getListCamps();
			camp = new Camp();
			JSFMessagers.addInfoMessage("Camp registration success");
			return "/admin/createCamp.xhtml";
		} catch (Exception e) {
			JSFMessagers.addErrorMessage("Failed to regsiter camp "+e.getMessage() +" &EX& "+e.getCause());
			return "/admin/createCamp.xhtml";
		}
	}

	
	public void provinceDitricts() {
		LocationImpl locationImpl = new LocationImpl();
		districts = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"DISTRICT"},
				"from Location where locationType = 'DISTRICT' AND parent.code = "+provCode);
	}
	
	public void districtSectors() {
		LocationImpl locationImpl = new LocationImpl();
		sectors = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"SECTOR"},
				"from Location where locationType = 'SECTOR' AND parent.code = "+distrCode);
	}
	
	public void sectorCells() {
		LocationImpl locationImpl = new LocationImpl();
		cells = locationImpl.getListLocationsWithQuery(new String[] {"locationType"}, new Object[] {"CELL"},
				"from Location where locationType = 'CELL' AND parent.code = "+sectCode);
	}
	
	
	public Camp getCamp() {
		return camp;
	}

	public void setCamp(Camp camp) {
		this.camp = camp;
	}

	public List<Location> getProvinces() {
		return provinces;
	}

	public void setProvinces(List<Location> provinces) {
		this.provinces = provinces;
	}

	public List<Location> getDistricts() {
		return districts;
	}

	public void setDistricts(List<Location> districts) {
		this.districts = districts;
	}

	public List<Location> getSectors() {
		return sectors;
	}

	public void setSectors(List<Location> sectors) {
		this.sectors = sectors;
	}

	public List<Location> getCells() {
		return cells;
	}

	public void setCells(List<Location> cells) {
		this.cells = cells;
	}

	public String getProvCode() {
		return provCode;
	}

	public void setProvCode(String provCode) {
		this.provCode = provCode;
	}

	public String getDistrCode() {
		return distrCode;
	}

	public void setDistrCode(String distrCode) {
		this.distrCode = distrCode;
	}

	public String getSectCode() {
		return sectCode;
	}

	public void setSectCode(String sectCode) {
		this.sectCode = sectCode;
	}

	public String getCellCode() {
		return cellCode;
	}

	public void setCellCode(String cellCode) {
		this.cellCode = cellCode;
	}


	public List<Camp> getCampsList() {
		return campsList;
	}

	public void setCampsList(List<Camp> campsList) {
		this.campsList = campsList;
	}
	
	
	
	
}

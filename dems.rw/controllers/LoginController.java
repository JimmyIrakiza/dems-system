package controllers;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import common.JSFBoundleProvider;
import common.JSFMessagers;
import common.SessionUtils;
import common.DbConstant;
import dao.impl.LocationImpl;
import dao.impl.LoginImpl;
import dao.impl.UserImpl;
import domain.User;

@ManagedBean
@SessionScoped
public class LoginController implements Serializable, DbConstant{
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	private String CLASSNAME = "LoginController :: ";
	private static final long serialVersionUID = 1L;
	/* to manage validation messages */
	private boolean isValid;
	/* end manage validation messages */
	private User user;
	/* begin class injection */
	JSFBoundleProvider provider = new JSFBoundleProvider();
	LoginImpl loginDao1 = new LoginImpl();
	UserImpl userImpl = new UserImpl();
	LocationImpl locationImpl = new LocationImpl();

	static User loggedInUser;
	/* end class injection */
	boolean isValidCredential;
	Timestamp timestamp = new Timestamp(Calendar.getInstance().getTime().getTime());
	
	@PostConstruct
	public void init() {
		if (user == null) {
			user = new User();
		}

	}
	
	public String validateUsernamePassword() {
		LOGGER.info(CLASSNAME + ":::step one");
		HttpSession session = SessionUtils.getSession();
		if(session.getAttribute("userSession")!=null) {
			session.invalidate();	
		}
		
		try {
			user = userImpl.getModelWithMyHQL(new String[] { USERNAME, PASSWORD },
					new Object[] { user.getUsername(), loginDao1.criptPassword(user.getPassword()) }, "from User");
			LOGGER.info("check username and password done :::" + user.getUsername());
			if (user != null) {
				isValidCredential = true;
				LOGGER.info("check username and password is correct:::");
			}else {
				isValidCredential = false;
				System.out.println("Changing Credintials ");
				setValid(false);
				JSFMessagers.addErrorMessage(getProvider().getValue("com.validat.user.password"));
				LOGGER.info("check username and password is incorrect:::");
			}
			
			LOGGER.info("::: USer Credi: "+ user.getUsername() + " And "+ user.getPassword());
			
			if (user != null && (user.getStatus().equalsIgnoreCase("active"))) {
			} else {
				isValidCredential = false;
			}
		} catch (Exception jj) {
			LOGGER.info("errrr::::::::: "+ jj.getCause()+ " ANd "+ jj.getMessage());
			setValid(false);
			JSFMessagers.addErrorMessage(getProvider().getValue("com.server.side.internal.error"));
			LOGGER.info(jj.getMessage());
			jj.printStackTrace();
			setValid(false);
			JSFMessagers.addErrorMessage(getProvider().getValue("com.server.side.internal.error"));
		}
		
		// Check if the account is locked
		
		System.out.println("Validity::: "+ isValidCredential);
		
		if(isValidCredential) {
			try {
				session.setAttribute("userSession", user);
				LOGGER.info(CLASSNAME + "Loging Save Login Historic");
				LOGGER.info("step 111");
				LOGGER.info(CLASSNAME + "Creating Sessions for users::" + user.getUsername());
				
			} catch (Exception ex) {
				LOGGER.info(CLASSNAME + "Loging Fail when login" + ex.getMessage());
				setValid(false);
				JSFMessagers.addInfoMessage("com.server.side.internal.error");
				ex.printStackTrace();
			}
			
			System.out.println("::::USER INFO: "+ user.getPosition());
			LOGGER.info("::::USER INFO: "+ user.getPosition());
			loggedInUser = user;
			if (user != null && user.getPosition().equalsIgnoreCase("admin")) {// ADMIN

				LOGGER.info(CLASSNAME + ":::ADMIN ");

				return "/templates/admin/adminDashboard.xhtml?faces-redirect=true";
				// return "";
			} else if (user != null && user.getPosition().equalsIgnoreCase("campManager")) {// CAMP Manager

				LOGGER.info(CLASSNAME + "::: CAMP Manager ");

				return "/templates/manager/managerDashboard.xhtml?faces-redirect=true";
				// return "";
			} else if (user != null && user.getPosition().equalsIgnoreCase("accountant")) {// Accountant

				LOGGER.info(CLASSNAME + "::: Accountant ");

				return "/templates/accountant/accountantDashboard.xhtml?faces-redirect=true";
				// return "";
			} else {

				LOGGER.info("login on 1 page for all users ");
				return "/public/homePage.xhtml?faces-redirect=true";
			}
		}else {
			LOGGER.info(CLASSNAME + "credential are invalid ");
			JSFMessagers.resetMessages();
			setValid(false);
			JSFMessagers.addErrorMessage(getProvider().getValue("com.validat.user.password"));
			return "";	
		}
	}

	public String logout() {
		HttpSession session = SessionUtils.getSession();

		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();

		String url = request.getContextPath() + "/login.xhtml";
		
		System.out.println("PATH: "+ url);
		
		try {
			User user= new User();
			user=(User)session.getAttribute("userSession");
			FacesContext.getCurrentInstance().getExternalContext().redirect(url);
			
		} catch (IOException e) {

			e.printStackTrace();
		}
		session.invalidate();
		return "";
	}
	
	public boolean isValid() {
		return isValid;
	}

	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public JSFBoundleProvider getProvider() {
		return provider;
	}

	public void setProvider(JSFBoundleProvider provider) {
		this.provider = provider;
	}

	public LoginImpl getLoginDao1() {
		return loginDao1;
	}

	public void setLoginDao1(LoginImpl loginDao1) {
		this.loginDao1 = loginDao1;
	}

	public UserImpl getUsersImpl() {
		return userImpl;
	}

	public void setUsersImpl(UserImpl usersImpl) {
		this.userImpl = usersImpl;
	}

	public static Logger getLogger() {
		return LOGGER;
	}

	public static User getLoggedInUser() {
		return loggedInUser;
	}

	public static void setLoggedInUser(User loggedInUser) {
		LoginController.loggedInUser = loggedInUser;
	}
	
	
	
}

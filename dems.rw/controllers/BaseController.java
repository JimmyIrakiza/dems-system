package controllers;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import dao.impl.UserImpl;

@ManagedBean
@SessionScoped
public class BaseController {
	public String getContextPath() {
		HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext()
				.getRequest();
		return request.getContextPath();
	}
	
	public String createAllTable() {
		UserImpl userImpl = new UserImpl();
		userImpl.creatingNewTable();
		System.out.println("TABLES CREATED SUCCESSFULL");
		return "";
	}
		
}

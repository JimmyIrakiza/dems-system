package dao.interfaces;

import java.util.List;

import domain.CampStaff;

public interface ICampStaff {
	public CampStaff saveCampStaffs(CampStaff campStaffs);

	public List<CampStaff> getListCampStaffs();

	public CampStaff gettCampStaffById(int campStaffId, String primaryKeyclomunName);

	public CampStaff UpdateCampStaffs(CampStaff campStaffs);

	public CampStaff getCampStaffsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

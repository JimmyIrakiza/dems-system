package dao.interfaces;

import java.util.List;

import domain.Donor;

public interface IDonor {
	public Donor saveDonors(Donor donors);

	public List<Donor> getListDonors();

	public Donor gettDonorById(int donorId, String primaryKeyclomunName);

	public Donor UpdateDonors(Donor donors);

	public Donor getDonorsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

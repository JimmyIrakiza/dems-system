package dao.interfaces;

import java.util.List;

import domain.Donation;

public interface IDonation {
	public Donation saveDonations(Donation donations);

	public List<Donation> getListDonations();

	public Donation gettDonationById(int donationId, String primaryKeyclomunName);

	public Donation UpdateDonations(Donation donations);

	public Donation getDonationsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

package dao.interfaces;

import java.util.List;

import domain.Location;
import domain.User;

public interface ILocation {
	public Location getLocationWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
	
	public List<Location> getListLocationsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
	
	public  List<Location> getLocations(String provCode, String locType);
}

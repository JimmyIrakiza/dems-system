package dao.interfaces;

import java.util.List;

import domain.Announcement;


public interface IAnnouncement {
	public Announcement saveAnnouncements(Announcement announcements);

	public List<Announcement> getListAnnouncements();

	public Announcement gettAnnouncementById(int announcementId, String primaryKeyclomunName);

	public Announcement UpdateAnnouncements(Announcement announcements);

	public Announcement getAnnouncementsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

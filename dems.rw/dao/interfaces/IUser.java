package dao.interfaces;

import java.util.List;

import domain.User;

public interface IUser {
	public User saveUsers(User users);

	public List<User> getListUsers();

	public User gettUserById(int userId, String primaryKeyclomunName);

	public User UpdateUsers(User users);

	public User getUsersWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

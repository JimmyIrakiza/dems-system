package dao.interfaces;

import java.util.List;

import domain.Child;

public interface IChild {
	public Child saveChild(Child child);

	public List<Child> getListChildren();

	public Child gettChildById(int childId, String primaryKeyclomunName);

	public Child UpdateChild(Child child);

	public Child getChildrenWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

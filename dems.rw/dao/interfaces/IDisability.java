package dao.interfaces;

import java.util.List;

import domain.Disability;

public interface IDisability {
	public Disability saveDisabilitys(Disability disabilitys);

	public List<Disability> getListDisabilitys();

	public Disability gettDisabilityById(int disabilityId, String primaryKeyclomunName);

	public Disability UpdateDisabilitys(Disability disabilitys);

	public Disability getDisabilitysWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

package dao.interfaces;

import java.util.List;

import domain.Family;

public interface IFamily {
	public Family saveFamily(Family family);

	public List<Family> getListFamilies();

	public Family gettFamilyById(int familyId, String primaryKeyclomunName);

	public Family UpdateFamily(Family family);

	public Family getFamilysWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

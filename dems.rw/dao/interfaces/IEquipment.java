package dao.interfaces;

import java.util.List;

import domain.Equipment;

public interface IEquipment {
	public Equipment saveEquipments(Equipment equipments);

	public List<Equipment> getListEquipments();

	public Equipment gettEquipmentById(int equipmentId, String primaryKeyclomunName);

	public Equipment UpdateEquipments(Equipment equipments);

	public Equipment getEquipmentsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

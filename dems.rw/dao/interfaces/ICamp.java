package dao.interfaces;

import java.util.List;

import domain.Camp;

public interface ICamp {
	public Camp saveCamps(Camp camp);

	public List<Camp> getListCamps();

	public Camp gettCampById(int campId, String primaryKeyclomunName);

	public Camp UpdateCamps(Camp camp);

	public Camp getCampsWithQuery(final String[] propertyName, final Object[] value, final String hqlStatement);
}

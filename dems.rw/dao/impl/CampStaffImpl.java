package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.ICampStaff;
import domain.Camp;
import domain.CampStaff;

public class CampStaffImpl extends AbstractDao<Integer, CampStaff> implements ICampStaff {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public CampStaff saveCampStaffs(CampStaff campStaffs) {
		return saveIntable(campStaffs);
	}

	@Override
	public List<CampStaff> getListCampStaffs() {
		return (List<CampStaff>) (Object) getModelList();
	}

	@Override
	public CampStaff gettCampStaffById(int campStaffId, String primaryKeyclomunName) {
		return (CampStaff) getModelById(campStaffId, primaryKeyclomunName);
	}

	@Override
	public CampStaff UpdateCampStaffs(CampStaff campStaffs) {
		return updateIntable(campStaffs);
	}

	@Override
	public CampStaff getCampStaffsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (CampStaff) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getCampsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

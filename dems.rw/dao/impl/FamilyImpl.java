package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IFamily;
import domain.Camp;
import domain.Donation;
import domain.Family;

public class FamilyImpl extends AbstractDao<Integer, Family> implements IFamily {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Family saveFamily(Family family) {
		return saveIntable(family);
	}

	@Override
	public List<Family> getListFamilies() {
		return (List<Family>) (Object) getModelList();
	}

	@Override
	public Family gettFamilyById(int familyId, String primaryKeyclomunName) {
		return (Family) getModelById(familyId, primaryKeyclomunName);
	}

	@Override
	public Family UpdateFamily(Family family) {
		return updateIntable(family);
	}

	@Override
	public Family getFamilysWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Family) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getFamilyWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IUser;
import domain.User;

public class UserImpl extends AbstractDao<Integer, User> implements IUser{
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());

	@Override
	public User saveUsers(User users) {
		return saveIntable(users);
	}

	@Override
	public List<User> getListUsers() {
		return (List<User>) (Object) getModelList();
		
	}

	@Override
	public User gettUserById(int userId, String primaryKeyclomunName) {
		return (User) getModelById(userId, primaryKeyclomunName);
	}

	
	public User UpdateUsers(User users) {
		return updateIntable(users);

	}

	@Override
	public User getUsersWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (User) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getUsersWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}
}

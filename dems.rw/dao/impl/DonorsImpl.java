package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IDonor;
import domain.Donor;

public class DonorsImpl extends AbstractDao<Integer, Donor> implements IDonor {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Donor saveDonors(Donor donors) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Donor> getListDonors() {
		return (List<Donor>) (Object) getModelList();
	}

	@Override
	public Donor gettDonorById(int donorId, String primaryKeyclomunName) {
		return (Donor) getModelById(donorId, primaryKeyclomunName);
	}

	@Override
	public Donor UpdateDonors(Donor donors) {
		return updateIntable(donors);
	}

	@Override
	public Donor getDonorsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Donor) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getDonorsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

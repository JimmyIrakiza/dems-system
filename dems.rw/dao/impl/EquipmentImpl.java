package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IEquipment;
import domain.Camp;
import domain.Donation;
import domain.Equipment;

public class EquipmentImpl extends AbstractDao<Integer, Equipment> implements IEquipment {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Equipment saveEquipments(Equipment equipments) {
		return saveIntable(equipments);
	}

	@Override
	public List<Equipment> getListEquipments() {
		return (List<Equipment>) (Object) getModelList();
	}

	@Override
	public Equipment gettEquipmentById(int equipmentId, String primaryKeyclomunName) {
		return (Equipment) getModelById(equipmentId, primaryKeyclomunName);
	}

	@Override
	public Equipment UpdateEquipments(Equipment equipments) {
		return updateIntable(equipments);
	}

	@Override
	public Equipment getEquipmentsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Equipment) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getEquipmentWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

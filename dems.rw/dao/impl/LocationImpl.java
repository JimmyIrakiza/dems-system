package dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.ILocation;
import domain.Location;

public class LocationImpl extends AbstractDao<String, Location> implements ILocation{
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Location getLocationWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Location) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getLocationsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

	@Override
	public List<Location> getListLocationsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return getListWithHQL(hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getlocationsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

	@Override
	public List<Location> getLocations(String parentCode, String locType) {
		configConnection();
		List<Location> locs = (List<Location>) (Object) getModelList();
		List<Location> locationResult = new ArrayList<Location>();
		for(Location loc: locs) {
			if(loc.getParent().getCode().equals(parentCode)&& loc.getLocationType().equals(locType)) {
				locationResult.add(loc);
			}
		}
		return locationResult;
	}

}

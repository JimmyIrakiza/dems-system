package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.ICamp;
import domain.Camp;
import domain.User;

public class CampImpl extends AbstractDao<Integer, Camp> implements ICamp {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Camp saveCamps(Camp camp) {
		return saveIntable(camp);
	}

	@Override
	public List<Camp> getListCamps() {
		return (List<Camp>) (Object) getModelList();
	}

	@Override
	public Camp gettCampById(int campId, String primaryKeyclomunName) {
		return (Camp) getModelById(campId, primaryKeyclomunName);
	}

	@Override
	public Camp UpdateCamps(Camp camp) {
		return updateIntable(camp);
	}

	@Override
	public Camp getCampsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Camp) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getCampsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

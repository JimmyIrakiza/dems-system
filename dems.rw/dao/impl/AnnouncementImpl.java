package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IAnnouncement;
import domain.Announcement;
import domain.Camp;

public class AnnouncementImpl extends AbstractDao<Integer, Announcement>implements IAnnouncement {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Announcement saveAnnouncements(Announcement announcements) {
		return saveIntable(announcements);
	}

	@Override
	public List<Announcement> getListAnnouncements() {
		return (List<Announcement>) (Object) getModelList();
	}

	@Override
	public Announcement gettAnnouncementById(int announcementId, String primaryKeyclomunName) {
		return (Announcement) getModelById(announcementId, primaryKeyclomunName);
	}

	@Override
	public Announcement UpdateAnnouncements(Announcement announcements) {
		return updateIntable(announcements);
	}

	@Override
	public Announcement getAnnouncementsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Announcement) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getAnnouncementsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

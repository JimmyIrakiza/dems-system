package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IDonation;
import domain.Camp;
import domain.Donation;

public class DonationImpl extends AbstractDao<Integer, Donation> implements IDonation {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Donation saveDonations(Donation donations) {
		return saveIntable(donations);
	}

	@Override
	public List<Donation> getListDonations() {
		return (List<Donation>) (Object) getModelList();
	}

	@Override
	public Donation gettDonationById(int donationId, String primaryKeyclomunName) {
		return (Donation) getModelById(donationId, primaryKeyclomunName);
	}

	@Override
	public Donation UpdateDonations(Donation donations) {
		return updateIntable(donations);
	}

	@Override
	public Donation getDonationsWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Donation) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getCampsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

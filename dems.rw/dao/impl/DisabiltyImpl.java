package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IDisability;
import domain.Camp;
import domain.Disability;

public class DisabiltyImpl extends AbstractDao<Integer, Disability> implements IDisability {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Disability saveDisabilitys(Disability disability) {
		return saveIntable(disability);
	}

	@Override
	public List<Disability> getListDisabilitys() {
		return (List<Disability>) (Object) getModelList();
	}

	@Override
	public Disability gettDisabilityById(int disabilityId, String primaryKeyclomunName) {
		return (Disability) getModelById(disabilityId, primaryKeyclomunName);
	}

	@Override
	public Disability UpdateDisabilitys(Disability disabilitys) {
		return updateIntable(disabilitys);
	}

	@Override
	public Disability getDisabilitysWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Disability) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getDisabilityWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

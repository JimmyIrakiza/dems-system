package dao.impl;

import java.util.List;
import java.util.logging.Logger;

import dao.generic.AbstractDao;
import dao.interfaces.IChild;
import domain.Camp;
import domain.Child;

public class ChildImpl extends AbstractDao<Integer, Child> implements IChild {
	private static final Logger LOGGER = Logger.getLogger(Thread.currentThread().getStackTrace()[0].getClassName());
	
	@Override
	public Child saveChild(Child child) {
		return saveIntable(child);
	}

	@Override
	public List<Child> getListChildren() {
		return (List<Child>) (Object) getModelList();
	}

	@Override
	public Child gettChildById(int childId, String primaryKeyclomunName) {
		return (Child) getModelById(childId, primaryKeyclomunName);
	}

	@Override
	public Child UpdateChild(Child child) {
		return updateIntable(child);
	}

	@Override
	public Child getChildrenWithQuery(String[] propertyName, Object[] value, String hqlStatement) {
		try {
			return (Child) getModelWithMyHQL(propertyName, value, hqlStatement);
		} catch (Exception ex) {
			LOGGER.info("getCampsWithQuery  Query error ::::" + ex.getMessage());
		}
		return null;
	}

}

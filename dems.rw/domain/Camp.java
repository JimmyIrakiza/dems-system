package domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class Camp {
	@Id@GeneratedValue
	private int id;
	private String campName;
	@Column(unique = true)
	private String campCode;
	private int capacity;
	@OneToMany
	private List<User> staff;
	@OneToMany
	private List<Family> families;
	@OneToOne
	private Location location;
	@OneToMany
	private List<CampStaff> campStaff;
	
	
	public List<CampStaff> getCampStaff() {
		return campStaff;
	}
	public void setCampStaff(List<CampStaff> campStaff) {
		this.campStaff = campStaff;
	}
	public String getCampCode() {
		return campCode;
	}
	public void setCampCode(String campCode) {
		this.campCode = campCode;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCampName() {
		return campName;
	}
	public void setCampName(String campName) {
		this.campName = campName;
	}
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	
	
	public List<User> getStaff() {
		return staff;
	}
	public void setStaff(List<User> staff) {
		this.staff = staff;
	}
	public List<Family> getFamilies() {
		return families;
	}
	public void setFamilies(List<Family> families) {
		this.families = families;
	}
	public Location getLocation() {
		return location;
	}
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public Camp() {
		super();
	}
	
	
}

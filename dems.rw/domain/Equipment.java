package domain;

import javax.persistence.*;

@Entity
public class Equipment {
	@Id@GeneratedValue
	private int id;
	@Column(unique = true)
	private String name;
	@Column(unique = true)
	private String code;
	private double cost;
	private double quantity;
	
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getQuantity() {
		return quantity;
	}
	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}
	
	
}

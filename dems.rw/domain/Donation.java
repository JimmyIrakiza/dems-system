package domain;

import javax.persistence.*;

@Entity
public class Donation {
	@Id@GeneratedValue
	private int id;
	private String status;
	@ManyToOne
	private Donor donatedBy;
	private double amount;
	private String currency;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Donor getDonatedBy() {
		return donatedBy;
	}
	public void setDonatedBy(Donor donatedBy) {
		this.donatedBy = donatedBy;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	
}

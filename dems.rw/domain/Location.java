package domain;



import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;


@Entity
public class Location implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
    @NotNull
    private Integer state;
    @NotNull
    private Integer version;
    @NotNull
    private String code;
    private String description;
    @Column(name = "location_type")
    @NotNull
    private String locationType;
    @NotNull
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Location parent;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parent")
    private Set<Location> locations = new HashSet<Location>(0);

    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocationType() {
        return locationType;
    }

    public void setLocationType(String locationType) {
        this.locationType = locationType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getParent() {
        return parent;
    }

    public void setParent(Location parent) {
        this.parent = parent;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    
    public Location() {
		super();
	}

	public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

	public Location(Integer id, Integer state, Integer version, String code, String description, String locationType,
			String name, Location parent, Set<Location> locations) {
		super();
		this.id = id;
		this.state = state;
		this.version = version;
		this.code = code;
		this.description = description;
		this.locationType = locationType;
		this.name = name;
		this.parent = parent;
		this.locations = locations;
	}

	
}

package domain;

import javax.persistence.*;

@Entity
public class Announcement {
	@Id@GeneratedValue
	private int id;
	private String announcedBy;
	private String title;
	private String content;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAnnouncedBy() {
		return announcedBy;
	}
	public void setAnnouncedBy(String announcedBy) {
		this.announcedBy = announcedBy;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	
	
}

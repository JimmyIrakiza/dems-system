package domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class Donor {
	@Id@GeneratedValue
	private int id;
	private String name;
	@Column(unique = true)
	private String nationalId;
	@Column(unique = true)
	private String passportId;
	private String country;
	@OneToMany
	private List<Donation> donations;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNationalId() {
		return nationalId;
	}
	public void setNationalId(String nationalId) {
		this.nationalId = nationalId;
	}
	public String getPassportId() {
		return passportId;
	}
	public void setPassportId(String passportId) {
		this.passportId = passportId;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public List<Donation> getDonations() {
		return donations;
	}
	public void setDonations(List<Donation> donations) {
		this.donations = donations;
	}
	
	
	
}

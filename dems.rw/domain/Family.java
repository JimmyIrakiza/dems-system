package domain;

import java.util.List;

import javax.persistence.*;

@Entity
public class Family {
	@Id@GeneratedValue
	private int id;
	@Column(unique = true)
	private String familyLeaderId;
	private String fatherName;
	private String motherName;
	@OneToMany
	private List<Child> children;
	private int childrenNumber;
	
	public int getChildrenNumber() {
		return childrenNumber;
	}
	public void setChildrenNumber(int childrenNumber) {
		this.childrenNumber = childrenNumber;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFamilyLeaderId() {
		return familyLeaderId;
	}
	public void setFamilyLeaderId(String familyLeaderId) {
		this.familyLeaderId = familyLeaderId;
	}
	public String getFatherName() {
		return fatherName;
	}
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	public String getMotherName() {
		return motherName;
	}
	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}
	public List<Child> getChildren() {
		return children;
	}
	public void setChildren(List<Child> children) {
		this.children = children;
	}
	
	
}

package domain;

import javax.persistence.*;

@Entity
public class Child {
	@Id@GeneratedValue
	private int id;
	private String name;
	private int age;
	private String educationLevel;
	private int classLevel;
	private String studiesOption;
	private String healthProblem;
	@ManyToOne
	private Family family;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getEducationLevel() {
		return educationLevel;
	}
	public void setEducationLevel(String educationLevel) {
		this.educationLevel = educationLevel;
	}
	public int getClassLevel() {
		return classLevel;
	}
	public void setClassLevel(int classLevel) {
		this.classLevel = classLevel;
	}
	public String getStudiesOption() {
		return studiesOption;
	}
	public void setStudiesOption(String studiesOption) {
		this.studiesOption = studiesOption;
	}
	public String getHealthProblem() {
		return healthProblem;
	}
	public void setHealthProblem(String healthProblem) {
		this.healthProblem = healthProblem;
	}
	public Family getFamily() {
		return family;
	}
	public void setFamily(Family family) {
		this.family = family;
	}
	
	
}
